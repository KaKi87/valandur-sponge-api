const
    { describe, before, it } = require('mocha'),
    chai = require('chai'),
    { expect } = chai,
    { Client, Player, World, Entity } = require('../lib'),
    {
        BASE_URL: baseUrl,
        API_KEY: apiKey,
        PLAYER_UUID: playerUuid,
        WORLD_UUID: worldUuid,
        ENTITY_UUID: entityUuid
    } = process.env;

chai.use(require('chai-as-promised'));

describe('Client', () => {
    let client;
    before('Instantiate client', () => client = new Client({ baseUrl, apiKey }));
    describe('Players', () => {
        describe('Fetch all', () => {
            it('Fetches all players', () => expect(client.players.fetchAll()).to.be.fulfilled);
            it('Fetched a player', function(){
                if(!client.players.size) return this.skip();
                const [[, player] = []] = client.players.cache;
                expect(player).to.be.an.instanceOf(Player);
                expect(player).to.have.all.keys(
                    'isFull', 'uuid', 'name', 'isOnline',
                    'positionX', 'positionY', 'positionZ',
                    'world',
                    'fetch'
                );
                expect(player.isFull).to.be.a('boolean');
                expect(player.isFull).to.be.false;
                expect(player.uuid).to.be.a('string');
                expect(player.name).to.be.a('string');
                expect(player.isOnline).to.be.a('boolean');
                expect(player.positionX).to.be.a('number');
                expect(player.positionY).to.be.a('number');
                expect(player.positionZ).to.be.a('number');
                expect(player.world).to.be.an.instanceOf(World);
                expect(player.fetch).to.be.a('function');
            });
        });
        (playerUuid ? describe : describe.skip)('Fetch one', () => {
            it('Fetches a player', () => expect(client.players.fetch(playerUuid)).to.be.fulfilled);
            it('Fetched the player', () => {
                const player = client.players.cache.get(playerUuid);
                expect(player.isFull).to.be.true;
                if(player.isOnline){
                    expect(player).to.include.keys(
                        'ip', 'port',
                        'rotationX', 'rotationY', 'rotationZ',
                        'velocityX', 'velocityY', 'velocityZ'
                    );
                    expect(player.ip).to.be.a('string');
                    expect(player.port).to.be.a('number');
                    expect(player.rotationX).to.be.a('number');
                    expect(player.rotationY).to.be.a('number');
                    expect(player.rotationZ).to.be.a('number');
                    expect(player.velocityX).to.be.a('number');
                    expect(player.velocityY).to.be.a('number');
                    expect(player.velocityZ).to.be.a('number');
                }
            });
        });
        (playerUuid ? describe : describe.skip)('Fetch self', () => it('Fetches self', () => {
            const player = client.players.cache.get(playerUuid);
            expect(player.fetch()).to.be.fulfilled;
        }));
    });
    describe('Worlds', () => {
        describe('Cache', () => {
            it('Cached a world', function(){
                if(!client.worlds.cache.size) return this.skip();
                const [[, world] = []] = client.worlds.cache;
                expect(world).to.be.an.instanceOf(World);
                expect(world).to.have.all.keys('isFull', 'uuid', 'name', 'isLoaded', 'fetch');
                expect(world.isFull).to.be.a('boolean');
                expect(world.isFull).to.be.false;
                expect(world.uuid).to.be.a('string');
                expect(world.name).to.be.a('string');
                expect(world.isLoaded).to.be.a('boolean');
            });
        });
        describe('Fetch all', () => {
            it('Fetches all worlds', () => expect(client.worlds.fetchAll()).to.be.fulfilled);
            it('Fetched a world', () => {
                const [[, world] = []] = client.worlds.cache;
                expect(world).to.be.an.instanceOf(World);
                expect(world).to.have.all.keys(
                    'isFull', 'uuid', 'name', 'isLoaded',
                    'fetch'
                );
                expect(world.isFull).to.be.a('boolean');
                expect(world.isFull).to.be.false;
                expect(world.uuid).to.be.a('string');
                expect(world.name).to.be.a('string');
                expect(world.isLoaded).to.be.a('boolean');
            });
        });
        (worldUuid ? describe : describe.skip)('Fetch one', () => {
            it('Fetches a world', () => expect(client.worlds.fetch(worldUuid)).to.be.fulfilled);
            it('Fetched the world', () => {
                const world = client.worlds.cache.get(worldUuid);
                expect(world.isFull).to.be.true;
            });
        });
        (worldUuid ? describe : describe.skip)('Fetch self', () => it('Fetches self', () => {
            const world = client.worlds.cache.get(worldUuid);
            expect(world.fetch()).to.be.fulfilled;
        }));
    });
    describe('Entities', () => {
        describe('Fetch all', () => {
            it('Fetches all entities', () => expect(client.entities.fetchAll()).to.be.fulfilled);
            it('Fetched a entity', () => {
                const [[, entity] = []] = client.entities.cache;
                expect(entity).to.be.an.instanceOf(Entity);
                expect(entity).to.have.all.keys(
                    'isFull', 'uuid', 'type',
                    'positionX', 'positionY', 'positionZ',
                    'world',
                    'fetch'
                );
                expect(entity.isFull).to.be.a('boolean');
                expect(entity.isFull).to.be.false;
                expect(entity.uuid).to.be.a('string');
                expect(entity.type).to.be.a('string');
                expect(entity.positionX).to.be.a('number');
                expect(entity.positionY).to.be.a('number');
                expect(entity.positionZ).to.be.a('number');
                expect(entity.world).to.be.an.instanceOf(World);
                expect(entity.fetch).to.be.a('function');
            });
        });
        (entityUuid ? describe : describe.skip)('Fetch one', () => {
            it('Fetches a entity', () => expect(client.entities.fetch(entityUuid)).to.be.fulfilled);
            it('Fetched the entity', () => {
                const entity = client.entities.cache.get(entityUuid);
                expect(entity.isFull).to.be.true;
            });
        });
        (entityUuid ? describe : describe.skip)('Fetch self', () => it('Fetches self', () => {
            const entity = client.entities.cache.get(entityUuid);
            expect(entity.fetch()).to.be.fulfilled;
        }));
    });
});
