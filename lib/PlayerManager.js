const Player = require('./Player.js');

/** @class PlayerManager */
function PlayerManager({
    _axios,
    _worlds
}){
    /** @type {Map<UUID, Player>} */
    this.cache = new Map();

    /**
     * @param {UUID} uuid
     * @returns {Promise<Player>}
     */
    this.fetch = async uuid => {
        const player = new Player({ _axios, _worlds, uuid });
        await player.fetch();
        this.cache.set(player.uuid, player);
        return player;
    };

    /** @returns {Promise<Map<UUID, Player>>} */
    this.fetchAll = async () => {
        (await _axios('/player')).data.forEach(playerData => {
            const player = new Player({ _axios, _worlds, data: playerData });
            this.cache.set(player.uuid, player);
        });
        return this.cache;
    };
}

module.exports = PlayerManager;