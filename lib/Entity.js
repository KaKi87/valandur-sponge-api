/** @class Entity */
function Entity({
    _axios,
    _worlds,
    uuid,
    data
}){
    const _patch = () => {
        /** @type {UUID} */
        this.uuid = data['uuid'];

        /** @type {String} */
        this.type = data['type']['id'];

        /** @type {Number} */
        this.positionX = data['location']['position']['x'];

        /** @type {Number} */
        this.positionY = data['location']['position']['y'];

        /** @type {Number} */
        this.positionZ = data['location']['position']['z'];

        /** @type {World} */
        this.world
            = _worlds.cache.get(data['location']['world']['uuid'])
            || _worlds.add(data['location']['world']['uuid'], data['location']['world']);
    };

    /** @returns {Promise<Entity>} */
    this.fetch = async () => {
        ({ data } = await _axios(`/entity/${this.uuid || uuid}`));
        this.isFull = true;
        _patch();
        return this;
    };

    this.isFull = false;

    if(data)
        _patch();
}

module.exports = Entity;