/** @class Player */
function Player({
    _axios,
    _worlds,
    uuid,
    data
}){
    const _patch = () => {
        /** @type {UUID} */
        this.uuid = data['uuid'];

        /** @type {String} */
        this.name = data['name'];

        /** @type {Boolean} */
        this.isOnline = data['online'];

        if(this.isOnline){
            /** @type {Number} */
            this.positionX = data['location']['position']['x'];

            /** @type {Number} */
            this.positionY = data['location']['position']['y'];

            /** @type {Number} */
            this.positionZ = data['location']['position']['z'];

            /** @type {World} */
            this.world
                = _worlds.cache.get(data['location']['world']['uuid'])
                || _worlds.add(data['location']['world']['uuid'], data['location']['world']);
        }

        if(this.isFull && this.isOnline){
            const [ip, port] = data['address'].slice(1).split(':');

            /** @type {String} */
            this.ip = ip;

            /** @type {Number} */
            this.port = parseInt(port);

            /** @type {Number} */
            this.rotationX = data['rotation']['x'];

            /** @type {Number} */
            this.rotationY = data['rotation']['y'];

            /** @type {Number} */
            this.rotationZ = data['rotation']['z'];

            /** @type {Number} */
            this.velocityX = data['velocity']['x'];

            /** @type {Number} */
            this.velocityY = data['velocity']['y'];

            /** @type {Number} */
            this.velocityZ = data['velocity']['z'];
        }
    };

    /** @returns {Promise<Player>} */
    this.fetch = async () => {
        ({ data } = await _axios(`/player/${this.uuid || uuid}`));
        this.isFull = true;
        _patch();
        return this;
    };

    this.isFull = false;

    if(data)
        _patch();
}

module.exports = Player;