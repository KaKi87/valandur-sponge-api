const
    axios = require('axios'),
    WorldManager = require('./WorldManager.js'),
    PlayerManager = require('./PlayerManager.js'),
    EntityManager = require('./EntityManager.js');

/**
 * @class Client
 * @param {String} baseUrl - Base API URL
 * @param {String} apiKey - API key
 */
function Client({
    baseUrl,
    apiKey
}){
    const _axios = axios.create();
    _axios.interceptors.request.use(config => {
        if(config.url.startsWith('/')) config = {
            ...config,
            url: baseUrl + '/api/v5' + config.url,
            headers: {
                ...config.headers,
                'X-WebAPI-Key': apiKey
            }
        };
        return config;
    });

    /** @type {WorldManager} */
    this.worlds = new WorldManager({ _axios });

    /** @type {PlayerManager} */
    this.players = new PlayerManager({ _axios, _worlds: this.worlds });

    /** @type {EntityManager} */
    this.entities = new EntityManager({ _axios, _worlds: this.worlds });
}

module.exports = Client;