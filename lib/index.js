/** @typedef {String} UUID */

const
    Client = require('./Client.js'),
    Player = require('./Player.js'),
    World = require('./World.js'),
    Entity = require('./Entity.js');

module.exports = {
    Client,
    Player,
    World,
    Entity
};