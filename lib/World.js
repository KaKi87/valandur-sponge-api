/** @class World */
function World({
    _axios,
    uuid,
    data
}){
    const _patch = () => {
        /** @type {UUID} */
        this.uuid = data['uuid'];

        /** @type {String} */
        this.name = data['name'];

        /** @type {Boolean} */
        this.isLoaded = data['loaded'];
    };

    /** @returns {Promise<World>} */
    this.fetch = async () => {
        ({ data } = await _axios(`/world/${this.uuid || uuid}`));
        this.isFull = true;
        _patch();
        return this;
    };

    this.isFull = false;

    if(data)
        _patch();
}

module.exports = World;