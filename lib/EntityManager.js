const Entity = require('./Entity.js');

/** @class EntityManager */
function EntityManager({
    _axios,
    _worlds
}){
    /** @type {Map<UUID, Entity>} */
    this.cache = new Map();

    /**
     * @param {UUID} uuid
     * @returns {Promise<Entity>}
     */
    this.fetch = async uuid => {
        const entity = new Entity({ _axios, _worlds, uuid });
        await entity.fetch();
        this.cache.set(entity.uuid, entity);
        return entity;
    };

    /** @returns {Promise<Map<UUID, Entity>>} */
    this.fetchAll = async () => {
        (await _axios('/entity')).data.forEach(entityData => {
            const entity = new Entity({ _axios, _worlds, data: entityData });
            this.cache.set(entity.uuid, entity);
        });
        return this.cache;
    };

    /**
     * @param {Number} minPositionX
     * @param {Number} minPositionY
     * @param {Number} minPositionZ
     * @param {Number} maxPositionX
     * @param {Number} maxPositionY
     * @param {Number} maxPositionZ
     * @return {Promise<[Entity]>}
     */
    this.fetchAllInRange = async ({
        minPositionX, minPositionY, minPositionZ,
        maxPositionX, maxPositionY, maxPositionZ
    }) => {
        const res = [];
        (await _axios('/entity', {
            params: {
                'min': `${Math.floor(minPositionX)}|${Math.floor(minPositionY)}|${Math.floor(minPositionZ)}`,
                'max': `${Math.floor(maxPositionX)}|${Math.floor(maxPositionY)}|${Math.floor(maxPositionZ)}`
            }
        })).data.forEach(entityData => {
            const entity = new Entity({ _axios, _worlds, data: entityData });
            this.cache.set(entity.uuid, entity);
            res.push(entity);
        });
        return res;
    };
}

module.exports = EntityManager;