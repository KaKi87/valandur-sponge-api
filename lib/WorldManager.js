const World = require('./World.js');

/** @class WorldManager */
function WorldManager({
    _axios
}){
    /** @type {Map<UUID, World>} */
    this.cache = new Map();

    /**
     * @param {UUID} uuid
     * @param {Object} data
     * @return {World}
     */
    this.add = (uuid, data) => {
        const world = new World({ _axios, data });
        this.cache.set(uuid, world);
        return world;
    };

    /**
     * @param {UUID} uuid
     * @returns {Promise<World>}
     */
    this.fetch = async uuid => {
        const world = new World({ _axios, uuid });
        await world.fetch();
        this.cache.set(world.uuid, world);
        return world;
    };

    /** @returns {Promise<Map<UUID, World>>} */
    this.fetchAll = async () => {
        (await _axios('/world')).data.forEach(worldData => {
            const world = new World({ _axios, data: worldData });
            this.cache.set(world.uuid, world);
        });
        return this.cache;
    };
}

module.exports = WorldManager;